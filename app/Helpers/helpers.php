<?php

use Illuminate\Support\Facades\Cache;

function adminUrl($file)
{
    return env('APP_ADMIN_PANEL_URL').'upload/admin/' . $file;
}

function getPageName($url,$status = null)
{
    if (empty($url))
    {
        return '#';
    }

    if ($status = 1)
    {
        $explode = explode('?'.config('app.page_name').'=',$url);

        return route('blog.index.page',$explode[1]);
    }

    return '/'.str_replace('?'.config('app.page_name').'=','/'.config('app.page_name').'/',$url);
}

function ckeditorImage($summary)
{
    return str_replace('/images', env('APP_ADMIN_PANEL_URL').'images',$summary);
}

function settings($key)
{
    if (!Cache::has($key))
    {
        $settings = \App\Models\Setting::where('key',$key)->first();

        Cache::put($key, $settings, now()->addMinutes(200));
    }

    return Cache::get($key)->value;
}