<?php

namespace App\Http\Middleware;

use Artesaos\SEOTools\Facades\SEOMeta;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class Variables
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        SEOMeta::setTitleDefault(settings('site.title'));
        SEOMeta::setDescription(settings('site.description'));

        View::share([
            'noindex' => 0,
        ]);

        return $next($request);
    }
}
