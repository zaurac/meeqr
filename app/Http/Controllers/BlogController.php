<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\SEOMeta;
use halilBelkir\WebConvert\ImageHelper as WebpConvert;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index($page = 1)
    {
        $data = Blog::where('status',1)->paginate(10,'*',config('app.page_name'),$page);

        SEOMeta::setTitle('Blog');
        OpenGraph::addImage(asset('assets/images/blog.jpg'));

        return view('blog.index',compact('data'));
    }

    public function detail($slug)
    {
        $value = Blog::Where('slug',$slug)->first();
        $image = WebpConvert::getImage(adminUrl($value->image),1920,500,$value->seo_title,1);

        SEOMeta::setTitle($value->seo_title);
        OpenGraph::addImage(asset($image));

        return view('blog.detail',compact('value'));
    }
}
