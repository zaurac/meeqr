<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\Customer;
use App\Models\Faq;
use App\Models\Form;
use App\Models\Package;
use App\Models\User;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\SEOMeta;
use Carbon\Carbon;
use halilBelkir\WebConvert\ImageHelper as WebpConvert;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Validator;

class MainController extends Controller
{
    public function index()
    {
        $packages = Package::where('status',1)->get();
        $faq      = Faq::where('status',1)->where('homepage',1)->orderBy('order','asc')->limit(5)->get();
        $image    = WebpConvert::getImage(adminUrl(settings('site.social_media_image')),1920,500,'meeqr',1);

        OpenGraph::addImage(asset($image));

        return view('index',compact('packages','faq'));
    }

    public function faq()
    {
        $faq = Faq::where('status',1)->orderBy('order','asc')->get();

        SEOMeta::setTitle('Sıkça Sorulan Sorular');
        OpenGraph::addImage(asset('assets/images/faq.jpg'));

        return view('faq',compact('faq'));
    }

    public function sitemap()
    {
        $blog    = Blog::where('status',1)->selectRaw("DATE_FORMAT(created_at, '%Y-%m-%d') as createdAt,DATE_FORMAT(updated_at, '%Y-%m-%d') as updatedAt,slug")->where('sitemap',1)->where('noindex',0)->get();
        $content = view('sitemap',compact('blog'));

        return response($content)->header('Content-Type', 'application/xml');
    }

    public function contact()
    {
        SEOMeta::setTitle('İletişim');
        OpenGraph::addImage(asset('assets/images/contact.jpg'));

        return view('contact');
    }

    public function form(Request $request)
    {
        try
        {
            $attribute =
                [
                    'email'        => 'E-Mail',
                    'name_surname' => 'Ad & Soyad',
                    'message'      => 'Mesajınız',
                ];

            $rules =
                [
                    'email'        => 'email|required',
                    'name_surname' => 'required',
                    'message'      => 'required',
                ];

            $validator = Validator::make($request->all(), $rules);
            $validator->setAttributeNames($attribute);

            if ($validator->fails())
            {
                return response()->json(
                    [
                        'result' => 2,
                        'message' => $validator->errors()
                    ],403
                );
            }

            $form               = new Form();
            $form->name_surname = $request->get('name_surname');
            $form->email        = $request->get('email');
            $form->message      = $request->get('message');
            $form->save();

            return response()->json(
                [
                    'result'  => 1,
                    'message' => 'Form Gönderilmiştir. Sayfa yenileniyor..',
                    'route'   => route('contact')
                ]
            );
        }
        catch (\Exception $e)
        {
            return response()->json(
                [
                    'result'  => 3,
                    'error'   => $e->getMessage(),
                    'message' => 'İşleminizi şimdi gerçekleştiremiyoruz. Daha sonra tekrar deneyiniz.'
                ], 403);
        }
    }

    public function register()
    {
        SEOMeta::setTitle('MeeQR\'a Katıl');
        OpenGraph::addImage(asset('assets/images/register.jpg'));

        return view('register');
    }

    public function registerStore(Request $request)
    {
        try
        {
            $attribute =
                [
                    'company_name' => 'Şirket Ünvanı',
                    'name_surname' => 'Ad & Soyad',
                    'phone'        => 'Telefon Numarası',
                    'email'        => 'E-Mail',
                    'password'     => 'Şifre',
                    'address'      => 'Adres',
                ];

            $rules =
                [
                    'company_name' => 'required',
                    'name_surname' => 'required',
                    'address'      => 'required',
                    'phone'        => 'required|unique:customers',
                    'email'        => 'email|required|unique:users',
                    'password'     => 'required|confirmed|min:8',
                ];

            $validator = Validator::make($request->all(), $rules);
            $validator->setAttributeNames($attribute);

            if ($validator->fails())
            {
                return response()->json(
                    [
                        'result' => 2,
                        'message' => $validator->errors()
                    ],403
                );
            }

            $customer                  = new Customer;
            $customer->title           = $request->get('company_name');
            $customer->slug            = Str::slug($request->get('company_name').$request->get('name_surname'),'-');
            $customer->email           = $request->get('email');
            $customer->phone           = $request->get('phone');
            $customer->address         = $request->get('address');
            $customer->demo_start_date = Carbon::now();
            $customer->demo_end_date   = Carbon::now()->addDay(14);
            $customer->status          = 1;
            $customer->payment_status  = 1;
            $customer->save();

            $user              = new User;
            $user->name        = $request->get('name_surname');
            $user->email       = $request->get('email');
            $user->customer_id = $customer->id;
            $user->password    = Hash::make($request->get('password'));
            $user->save();

            return response()->json(
                [
                    'result'  => 1,
                    'message' => 'İşlem Başarılı. Yönlendiriliyorsunuz...',
                    'route'   => env('APP_CUSTOMER_PANEL_URL')
                ]
            );
        }
        catch (\Exception $e)
        {
            return response()->json(
                [
                    'result'  => 3,
                    'error'   => $e->getMessage(),
                    'message' => 'İşleminizi şimdi gerçekleştiremiyoruz. Daha sonra tekrar deneyiniz.'
                ], 403);
        }
    }

    public function encodeHash($value)
    {
        return Crypt::encryptString(Crypt::encryptString($value));
    }
}
