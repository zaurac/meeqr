(function ($) {
	"use strict";

	var win = $(window);
	$('#mobile-menu').meanmenu({
		meanMenuContainer: '.mobile-menu',
		meanScreenWidth: "991"
	});
	win.on('load', function () {
		$('#loading').hide();
	})
	$('.open-mobile-menu').on('click', function () {
		$('.side-info').addClass('info-open');
		$('.offcanvas-overlay').addClass('overlay-open');
	})

	$('.side-info-close,.offcanvas-overlay,.mobile_one_page li.menu-item a.nav-link').on('click', function () {
		$('.side-info').removeClass('info-open');
		$('.offcanvas-overlay').removeClass('overlay-open');
	})


	win.on('scroll', function () {
		var scroll = win.scrollTop();
		if (scroll < 245) {
			$(".header-sticky").removeClass("sticky");
		} else {
			$(".header-sticky").addClass("sticky");
		}
	});

	if ($(".slider-active").length > 0) {
		let sliderActive1 = '.slider-active';
		let sliderInit1 = new Swiper(sliderActive1, {
			slidesPerView: 1,
			slidesPerColumn: 1,
			paginationClickable: true,
			loop: true,
			effect: 'fade',

			autoplay: {
				delay: 5000,
			},
			pagination: {
				el: '.swiper-pagination',
				type: 'fraction',
			},
			navigation: {
				nextEl: '.slide-next',
				prevEl: '.slide-prev',
			},

			a11y: false
		});

		function animated_swiper(selector, init) {
			let animated = function animated() {
				$(selector + ' [data-animation]').each(function () {
					let anim = $(this).data('animation');
					let delay = $(this).data('delay');
					let duration = $(this).data('duration');

					$(this).removeClass('anim' + anim)
						.addClass(anim + ' animated')
						.css({
							webkitAnimationDelay: delay,
							animationDelay: delay,
							webkitAnimationDuration: duration,
							animationDuration: duration
						})
						.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
							$(this).removeClass(anim + ' animated');
						});
				});
			};
			animated();
			init.on('slideChange', function () {
				$(sliderActive1 + ' [data-animation]').removeClass('animated');
			});
			init.on('slideChange', animated);
		}

		animated_swiper(sliderActive1, sliderInit1);
	}
	if ($(".testimonial-active").length > 0) {
		let swipertestimonial = new Swiper('.testimonial-active', {
			slidesPerView: 1,
			spaceBetween: 30,
			loop: true,
			infinite: false,
			autoplay: {
				delay: 5000,
			},
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
			},
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			scrollbar: {
				el: '.swiper-scrollbar',
				dynamicBullets: true,
			}

		});
	}

	if ($(".brand-active").length > 0) {
		let swiperbrand = new Swiper('.brand-active', {
			slidesPerView: 5,
			spaceBetween: 30,
			loop: true,
			infinite: false,
			autoplay: {
				delay: 5000,
			},


			breakpoints: {
				320: {
					slidesPerView: 3,
				},
				480: {
					slidesPerView: 4,
				},
				768: {
					slidesPerView: 4,
				},
				1200: {
					slidesPerView: 5,
				},
				1400: {
					slidesPerView: 5,
				},
			}

		});
	}
	if ($(".sasup-sm-brand-active_1").length > 0) {
		let swiperBrandActive = new Swiper('.sasup-sm-brand-active_1', {
			slidesPerView: 6,
			spaceBetween: 30,
			loop: true,
			infinite: true,
			autoplay: {
				delay: 5000,
			},
			pagination: {
				el: '.swiper-brand-paginate',
				clickable: true,
			},
			navigation: {
				nextEl: '.swiper-sm-brand-next',
				prevEl: '.swiper-sm-brand-prev',
			},
			scrollbar: {
				el: '.swiper-scrollbar',
				dynamicBullets: true,
			},
			breakpoints: {
				320: {
					slidesPerView: 2,
				},
				576: {
					slidesPerView: 3,
				},
				768: {
					slidesPerView: 4,
				},
				992: {
					slidesPerView: 4,
				},
				1200: {
					slidesPerView: 5,
				},
				1400: {
					slidesPerView: 5
				}
			}

		});
	}
	if ($(".sasup-sm-brand-active").length > 0) {
		let swiperBrandActive = new Swiper('.sasup-sm-brand-active', {
			slidesPerView: 6,
			spaceBetween: 30,
			loop: true,
			infinite: true,
			autoplay: {
				delay: 5000,
			},
			pagination: {
				el: '.swiper-brand-paginate',
				clickable: true,
			},
			navigation: {
				nextEl: '.swiper-sm-brand-next',
				prevEl: '.swiper-sm-brand-prev',
			},
			scrollbar: {
				el: '.swiper-scrollbar',
				dynamicBullets: true,
			},
			breakpoints: {
				320: {
					slidesPerView: 3,
				},
				576: {
					slidesPerView: 3,
				},
				768: {
					slidesPerView: 4,
				},
				992: {
					slidesPerView: 4,
				},
				1200: {
					slidesPerView: 6,
				},
				1400: {
					slidesPerView: 6
				}
			}

		});
	}
	if ($(".news-active-2").length > 0) {
		let swipernews = new Swiper('.news-active-2', {
			slidesPerView: 5,
			spaceBetween: 30,
			loop: true,
			infinite: false,
			autoplay: {
				delay: 5000,
			},
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
			},
			navigation: {
				nextEl: '.news-swiper-button-next',
				prevEl: '.news-swiper-button-prev',
			},
			scrollbar: {
				el: '.swiper-scrollbar',
				dynamicBullets: true,
			},
			breakpoints: {
				320: {
					slidesPerView: 1,
					spaceBetween: 0,
				},
				576: {
					slidesPerView: 2,
					spaceBetween: 10,
				},
				992: {
					slidesPerView: 3,
				},
				1200: {
					slidesPerView: 3,
				},
				1400: {
					slidesPerView: 4,
				},
			}

		});
	}

	if ($(".testimonial-active-2").length > 0) {
		let swipertestimonial2 = new Swiper('.testimonial-active-2', {
			slidesPerView: 3,
			spaceBetween: 30,
			loop: true,
			infinite: false,
			autoplay: {
				delay: 5000,
			},
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
			},
			navigation: {
				nextEl: '.testi-swiper-button-next-2',
				prevEl: '.testi-swiper-button-prev-2',
			},
			scrollbar: {
				el: '.swiper-scrollbar',
				dynamicBullets: true,
			},
			breakpoints: {
				320: {
					slidesPerView: 1,
					spaceBetween: 0,
				},
				576: {
					slidesPerView: 1,
				},
				992: {
					slidesPerView: 3,
				},
				1200: {
					slidesPerView: 3,
				},
				1400: {
					slidesPerView: 3,
				},
			}

		});
	}

	if ($(".sasup-testimonial-active-4").length > 0) {
		let swipertestimonial4 = new Swiper('.sasup-testimonial-active-4', {
			slidesPerView: 1,
			spaceBetween: 0,
			loop: true,
			infinite: false,
			autoplay: {
				delay: 5000,
			},
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
			},
			navigation: {
				nextEl: '.testimonial-swiper-button-next-4',
				prevEl: '.testimonial-swiper-button-prev-4',
			},
			scrollbar: {
				el: '.swiper-scrollbar',
				dynamicBullets: true,
			},
		});
	}

	if ($(".testimonial-active-4").length > 0) {
		let swipertestimonial4 = new Swiper('.testimonial-active-4', {
			slidesPerView: 3,
			spaceBetween: 30,
			loop: true,
			infinite: false,
			autoplay: {
				delay: 5000,
			},
			pagination: {
				el: '.swiper-pagination-4',
				clickable: true,
			},
			navigation: {
				nextEl: '.swiper-button-next-4',
				prevEl: '.swiper-button-prev-4',
			},
			scrollbar: {
				el: '.swiper-scrollbar-4',
				dynamicBullets: true,
			},
			breakpoints: {
				320: {
					slidesPerView: 1,
					spaceBetween: 0,
				},
				576: {
					slidesPerView: 1,
					spaceBetween: 10,
				},
				768: {
					slidesPerView: 2
				},
				992: {
					slidesPerView: 3,
				},
				1200: {
					slidesPerView: 3,
				},
				1400: {
					slidesPerView: 3,
				},
			}

		});
	}

	if ($(".brand-active-2").length > 0) {
		let swiperbrand2 = new Swiper('.brand-active-2', {
			slidesPerView: 6,
			spaceBetween: 30,
			loop: true,
			infinite: false,
			autoplay: {
				delay: 5000,
			},
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
			},
			navigation: {
				nextEl: '.testi-swiper-button-next-2',
				prevEl: '.testi-swiper-button-prev-2',
			},
			scrollbar: {
				el: '.swiper-scrollbar',
				dynamicBullets: true,
			},
			breakpoints: {
				320: {
					slidesPerView: 2,
					spaceBetween: 0,
				},
				576: {
					slidesPerView: 4,
				},
				992: {
					slidesPerView: 5,
				},
				1200: {
					slidesPerView: 5,
				},
				1400: {
					slidesPerView: 6,
				},
			}

		});
	}

	if ($(".portfolio-swiper-active").length > 0) {
		let portfolioActive = new Swiper('.portfolio-swiper-active', {
			slidesPerView: 3,
			spaceBetween: 30,
			loop: true,
			infinite: false,
			autoplay: {
				delay: 5000,
			},
			pagination: {
				el: '.swiper-portfolio-pagination',
				clickable: true,
			},
			navigation: {
				nextEl: '.portfolio-next',
				prevEl: '.portfolio-prev',
			},
			scrollbar: {
				el: '.swiper-scrollbar',
				dynamicBullets: true,
			},
			breakpoints: {
				320: {
					slidesPerView: 1,
					spaceBetween: 0,
				},
				576: {
					slidesPerView: 2,
				},
				992: {
					slidesPerView: 2,
				},
				1200: {
					slidesPerView: 3,
				},
				1400: {
					slidesPerView: 3,
				},
			}

		});
	}

	$("[data-background]").each(function () {
		$(this).css("background-image", "url(" + $(this).attr("data-background") + ")")
	})


	$("[data-width]").each(function () {
		$(this).css("width", $(this).attr("data-width"))
	})

	$("[data-color]").each(function () {
		$(this).css("color", $(this).attr("data-color"))
	})

	$("[data-bg-color]").each(function () {
		$(this).css("background-color", $(this).attr("data-bg-color"))
	})
	$('.sasup-accordion-item button.collapsed').parent().removeClass('sasup-disabled-item');
	$('.sasup-accordion-item button').on('click', function () {
		$('.sasup-accordion-item button').closest('.sasup-accordion-item').addClass('sasup-active-item');
		$('.sasup-accordion-item button').closest('.sasup-accordion-item').removeClass('sasup-disabled-item');
	})
	$('.sasup-accordion-item button.collapsed').on('click', function () {
		$('.sasup-accordion-item button.collapsed').closest('.sasup-accordion-item').addClass('sasup-disabled-item');
		$('.sasup-accordion-item button.collapsed').closest('.sasup-accordion-item').removeClass('sasup-active-item');
	})

	$('.popup-image').magnificPopup({
		type: 'image',
		gallery: {
			enabled: true
		}
	});

	$('.popup-video-link').magnificPopup({
		type: 'iframe'
	});

	$.scrollUp({
		scrollName: 'scrollUp',
		topDistance: '300',
		topSpeed: 300,
		animation: 'fade',
		animationInSpeed: 200,
		animationOutSpeed: 200,
		scrollText: '<i class="icofont icofont-long-arrow-up"></i>',
		activeOverlay: false,
	});

	$(".sasup-hero-form").on('click', function () {
		$(".epix-hero-form-label").addClass('epix-hero-form-clicked');
	})

	$(".mobile-bar-control").on('click', function () {
		$(this).addClass('bar-control-clicked');
		$(".responsive-sidebar").addClass('responsive-sidebar-visible');
	});
	$(".responsive-sidebar-close").on("click", function () {
		$(".responsive-sidebar").removeClass("responsive-sidebar-visible");
		$(".mobile-bar-control").removeClass("bar-control-clicked");
	});
	$('.monthly-tab-btn').on('click', function () {
		$('.annual-tab-btn').addClass('pos-left-after');
	})
	$('.annual-tab-btn').on('click', function () {
		$('.annual-tab-btn').removeClass('pos-left-after');
	})

	if ($(".stuff").length) {
		var stuff = $('.stuff').get(0);
		var parallaxInstance = new Parallax(stuff);
	}
	if ($(".stuff2").length) {
		var stuff2 = $('.stuff2').get(0);
		var parallaxInstance = new Parallax(stuff2);
	}
	if ($(".stuff3").length) {
		var stuff3 = $('.stuff3').get(0);
		var parallaxInstance = new Parallax(stuff3);
	}
	if ($(".stuff4").length) {
		var stuff4 = $('.stuff4').get(0);
		var parallaxInstance = new Parallax(stuff4);
	}
	if ($(".stuff5").length) {
		var stuff5 = $('.stuff5').get(0);
		var parallaxInstance = new Parallax(stuff5);
	}
	if ($(".stuff6").length) {
		var stuff6 = $('.stuff6').get(0);
		var parallaxInstance = new Parallax(stuff6);
	}

	$("a.clickup").on('click', function (event) {
		if (this.hash !== "") {
			event.preventDefault();
			var hash = this.hash;
			$('html, body').animate({
				scrollTop: $(hash).offset().top
			}, 500, function () {
				window.location.hash = hash;
			});
		}
	});



	$('.portfolio-menu button').on('click', function (event) {
		$(this).siblings('.active').removeClass('active');
		$(this).addClass('active');
		event.preventDefault();
	});
})(jQuery);