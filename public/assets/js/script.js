if ($('form').length > 0)
{
    $('form').submit(function (e)
    {
        e.preventDefault();

        let formData         = new FormData(this);
        let button           = $('button[type="submit"]:focus', this);
        let loadingText      = 'Yükleniyor...';
        let buttonText       = button.text();
        let responseSelector = $(this).attr('id');

        button.text(loadingText);

        $('#'+responseSelector+' .is-invalid').removeClass('is-invalid');
        $('#'+responseSelector+' .is-valid').removeClass('is-valid');
        getMessage('#'+ responseSelector+' .responseMessage',null,null,1);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: $(this).attr('method'),
            url:  $(this).attr('action'),
            data: formData,
            dataType: "json",
            cache:false,
            contentType: false,
            processData: false,
            success: function (response)
            {
                button.text(buttonText);

                getMessage('#'+ responseSelector+' .responseMessage',response.message,'success');

                setTimeout(function() { location = response.route; location}, 2500);
            },
            error : function (response)
            {
                button.text(buttonText);
                $("#"+responseSelector+" .invalid-feedback").remove();

                if (response.responseJSON.result == 2)
                {
                    $.each(response.responseJSON.message, function(i, item)
                    {
                        $('#'+responseSelector+' [name="'+i+'"]').addClass('is-invalid');
                        $('#'+responseSelector+' [name="'+i+'"]').closest('div.form-group').append('<div class="invalid-feedback">'+item[0]+'</div>');
                    });
                }
                else if (response.responseJSON.message)
                {
                    getMessage('#'+ responseSelector+' .responseMessage',response.responseJSON.message,'danger');
                }
                else
                {
                    getMessage('#'+ responseSelector+' .responseMessage',response.message,'danger');
                }
            }
        });
    });
}

function getMessage(selector,message,statusClass,status)
{
    if (status == 1)
    {
        document.querySelectorAll(selector)[0].innerHTML = '';
    }
    else
    {
        let html = '<div class="alert alert-'+statusClass+'">'+message+'</div>';

        document.querySelectorAll(selector)[0].innerHTML = html;

        $('html,body').animate({scrollTop: ($(selector).offset().top - 110)}, 200);
    }
}