<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\MainController::class,'index'])->name('index');
Route::get('iletisim', [\App\Http\Controllers\MainController::class,'contact'])->name('contact');
Route::get('site-haritasi', [\App\Http\Controllers\MainController::class,'sitemap'])->name('sitemap');
Route::get('sikca-sorulan-sorular', [\App\Http\Controllers\MainController::class,'faq'])->name('faq');
Route::post('form', [\App\Http\Controllers\MainController::class,'form'])->name('contact.store');
Route::get('kayit-ol', [\App\Http\Controllers\MainController::class,'register'])->name('register');
Route::post('kayit-ol-store', [\App\Http\Controllers\MainController::class,'registerStore'])->name('register.store');
Route::get('blog', [\App\Http\Controllers\BlogController::class,'index'])->name('blog.index');
Route::get('blog/'.config('app.page_name').'/{page?}', [\App\Http\Controllers\BlogController::class,'index'])->name('blog.index.page');
Route::get('blog/{slug}', [\App\Http\Controllers\BlogController::class,'detail'])->name('blog.detail');
Route::get('/clear-cache', function() {echo Artisan::call('cache:clear') ?  'bir hata oldu' : 'cache temizlendi.';  return redirect()->back(); });
