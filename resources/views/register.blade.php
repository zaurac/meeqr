@extends('layout.main')
@section('content')
    <div class="brandcrumb-area-2 breadcrumb-area-space-2  mb-120" data-overlay="theme-2" data-opacity="7" data-background="{{asset('assets/images/register.jpg')}}">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="breadcrumb-content text-center">
                        <h3 class="service-breadcrumb-title" data-color="#fff">MeeQR'a Katıl </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="df-sign-up__area pb-115">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-8">
                    <div class="df-booking2__form wow fadeInUp" data-wow-delay=".3s">
                        <div class="login__title-wrapper text-center mb-50">
                            <h3 class="login__title">Kayıt ol</h3>
                            <p class="mt-15">Bilgileri eksiksiz doldurunuz.</p>
                        </div>
                        <form action="{{route('register.store')}}" id="signUp" method="post">
                            <div class="responseMessage"></div>
                            <div class="row gx-5">
                                <div class="col-md-12 form-group mb-3">
                                    <input type="text" name="company_name" placeholder="Şirket Ünvanı *">
                                </div>

                                <div class="col-md-12 form-group mb-3">
                                    <input type="text" name="name_surname" placeholder="Ad & Soyad *">
                                </div>

                                <div class="col-md-12 form-group mb-3">
                                    <input type="email" name="email" placeholder="E-mail *">
                                </div>

                                <div class="col-md-12 form-group mb-3">
                                    <input type="text" name="phone" placeholder="Telefon Numarası *">
                                </div>

                                <div class="col-md-12 form-group mb-3">
                                    <textarea name="address" cols="30" rows="5" placeholder="Adres *"></textarea>
                                </div>

                                <div class="col-md-12 form-group mb-3">
                                    <input type="password" name="password" placeholder="Şifre *">
                                </div>

                                <div class="col-md-12 form-group mb-3">
                                    <input type="password" name="password_confirmation" placeholder="Şifre Tekrar *">
                                </div>

                                <div class="col-md-12">
                                    <div class="df-booking2__form-btn mt-15 mb-30">
                                        <button type="submit" class="sasup-theme-btn-2 w-100">Kayıt Ol</button>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="sing-up-text text-center">
                                            <span class="sign-title">
                                                Hesabın var mı?
                                            </span>
                                        <a class="sign-link" href="{{env('APP_CUSTOMER_PANEL_URL')}}" target="_blank">Giriş yap</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection