@extends('layout.main')
@section('content')
    <div class="brandcrumb-area-2 breadcrumb-area-space-2  mb-120" data-overlay="theme-2" data-opacity="7" data-background="{{asset('assets/images/contact.jpg')}}">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="breadcrumb-content text-center">
                        <h1 class="service-breadcrumb-title" data-color="#fff">İletişim </h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="contact-area">
        <div class="container">
            <div class="row pb-140 justify-content-between">
                <div class="col-xxl-5 col-xl-6 col-lg-6 mb-40 mb-lg-0">
                    <div class="contact-left-info">
                        <div class="contact-info-left-top mb-30">
                            <h2 class="contact-info-title">İletişim Bilgileri</h2>
                            <span class="contact-info-subtitle"> Merak ettiklerinizi sorabilirsiniz.</span>
                        </div>
                        <div class="contact-left-list-wrapper">
                            <div class="single-contact-left-item mb-10">
                                <div class="icon">
                                    <i class="fal fa-phone-alt"></i>
                                </div>
                                <div class="content">
                                    <h2 class="single-contact-left-label">Telefon Numaramız</h2>
                                    <span class="single-contact-left-info">
                                        <a href="tel:{{str_replace(' ','',settings('site.phone'))}}" class="__cf_email__">
                                            {{settings('site.phone')}}
                                        </a>
                                    </span>
                                </div>
                            </div>
                            <div class="single-contact-left-item mb-10">
                                <div class="icon">
                                    <i class="fal fa-envelope"></i>
                                </div>
                                <div class="content">
                                    <h2 class="single-contact-left-label">E-mail Adresimiz</h2>
                                    <span class="single-contact-left-info">
                                        <a href="mailto:{{settings('site.email')}}" class="__cf_email__">
                                            {{settings('site.email')}}
                                        </a>
                                    </span>
                                </div>
                            </div>
                            <div class="single-contact-left-item mb-10">
                                <div class="icon">
                                    <i class="fal fa-map-marker-alt"></i>
                                </div>
                                <div class="content">
                                    <h2 class="single-contact-left-label">Adresimiz</h2>
                                    <span class="single-contact-left-info">{{settings('site.address')}}</span>
                                </div>
                            </div>
                            @if(!empty(settings('site.facebook')) && !empty(settings('site.instagram')) && !empty(settings('site.twitter')) && !empty(settings('site.youtube')))
                                <div class="single-contact-left-item mb-10">
                                    <div class="icon">
                                        <i class="fal fa-share-alt"></i>
                                    </div>
                                    <div class="content">
                                        <h4 class="single-contact-left-label contact-left-label-space">Sosyal Medya Hesaplarımız</h4>
                                        <div class="share-social">
                                            @if(!empty(settings('site.facebook'))) <a href="{{settings('site.facebook')}}" target="_blank" data-color="#1877f2"><i class="fab fa-facebook-square"></i></a> @endif
                                            @if(!empty(settings('site.instagram'))) <a href="{{settings('site.instagram')}}" target="_blank" data-color="#833AB4"><i class="fab fa-instagram-square"></i></a> @endif
                                            @if(!empty(settings('site.twitter'))) <a href="{{settings('site.twitter')}}" target="_blank" data-color="#1da1f2"><i class="fab fa-twitter-square"></i></a> @endif
                                            @if(!empty(settings('site.youtube'))) <a href="{{settings('site.youtube')}}" target="_blank" data-color="#c4302b"><i class="fab fa-youtube-square"></i></a> @endif
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-xxl-6 col-xl-6 col-lg-6">
                    <div class="contact-form wow fadeInUp mb-50 mb-xl-0" data-wow-delay=".2s">
                        <form action="{{route('contact.store')}}" method="post" class="row" id="contact">
                            <div class="responseMessage"></div>
                            <div class="col-xl-6 col-md-6 form-group mb-3 post-input post-input-2">
                                <label for="name" class="post-input-label-defualt">Ad & Soyad *</label>
                                <input type="text" name="name_surname" placeholder="Ad & Soyad">
                            </div>

                            <div class="col-xl-6 col-md-6 form-group mb-3 post-input post-input-2">
                                <label for="email" class="post-input-label-defualt">E-mail *</label>
                                <input type="email" name="email" id="email" placeholder="E-mail">
                            </div>

                            <div class="col-xl-12 form-group mb-3 post-input has-textarea">
                                <label for="comment" class="post-input-label-defualt">Mesaj *</label>
                                <textarea id="comment" name="message" placeholder="Mesajını yazınız..."></textarea>
                            </div>

                            <div class="col-12">
                                <button type="submit" class="sasup-theme-btn sasup-theme-btn-2 transition-5">Mesajı Gönder</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="contact-map">
            {!! settings('site.map') !!}
        </div>
    </div>
@endsection