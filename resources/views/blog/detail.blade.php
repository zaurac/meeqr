@extends('layout.main')
@section('content')
    <div class="brandcrumb-area-2 breadcrumb-area-space-2 mb-60" data-overlay="theme-2" data-opacity="7" data-background="{{adminUrl($value->image)}}">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="breadcrumb-content text-center">
                        <h1 class="service-breadcrumb-title" data-color="#fff">{{$value->title}} </h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="blog-area">
        <div class="container">
            <div class="row">
                <div class="col-xxl-12 col-xl-12 col-lg-12">
                    <div class="related-news-area">
                        <div class="row g-20">
                            <div class="col-12 detail">
                                {!! $value->content !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection