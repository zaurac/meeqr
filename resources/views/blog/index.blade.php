@extends('layout.main')
@section('content')
    <div class="brandcrumb-area-2 breadcrumb-area-space-2  mb-80" data-overlay="theme-2" data-opacity="7" data-background="{{asset('assets/images/blog.jpg')}}">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="breadcrumb-content text-center">
                        <h1 class="service-breadcrumb-title" data-color="#fff">Blog </h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="blog-area">
        <div class="container">
            <div class="row">
                <div class="col-xxl-12 col-xl-12 col-lg-12">
                    <div class="related-news-area pb-50">
                        <div class="row g-20">
                            @if($data->total() > 0)
                                @foreach($data->items() as $value)
                                    <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-6 mb-30">
                                        <div class="related-news-box wow fadeInUp" data-wow-delay=".2s">
                                            <div class="thumb">
                                                <img src="{{adminUrl($value->image)}}" alt="image not found">
                                            </div>
                                            <div class="content">
                                                <div class="news-meta meta-related-space">
                                                    <div class="single-meta meta-sm">
                                                        <span class="news-date">{{\Carbon\Carbon::make($value->created_at)->format('d-m-Y')}}</span>
                                                    </div>
                                                </div>
                                                <a href="{{route('blog.detail',$value->slug)}}">
                                                    <h2 class="related-news-title">
                                                        {{$value->title}}
                                                    </h2>
                                                </a>
                                                <a href="{{route('blog.detail',$value->slug)}}" class="related-read-more">
                                                    Devamını Oku
                                                    <i class="fal fa-arrow-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                                <div class="row text-center">
                                    <div class="col-xxl-12">
                                        <div class="basic-pagination mt-40">
                                            <ul>
                                                @if(!empty($data->previousPageUrl()))
                                                    <li>
                                                        <a class="page-numbers prev" href="{{getPageName($data->previousPageUrl(),1)}}">
                                                            <i class="fal fa-arrow-left"></i>
                                                        </a>
                                                    </li>
                                                @endif

                                                @foreach($data->links()->elements[0] as $key => $link)
                                                    <li>
                                                        <a href="{{route('blog.index.page',$key)}}" class="page-numbers {{$data->currentPage() == $key ? 'current' : null}}">
                                                            {{$key}}
                                                        </a>
                                                    </li>
                                                @endforeach

                                                @if(!empty($data->nextPageUrl()))
                                                    <li>
                                                        <a class="page-numbers next" href="{{getPageName($data->nextPageUrl(),1)}}">
                                                            <i class="fal fa-arrow-right"></i>
                                                        </a>
                                                    </li>
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="alert alert-info"> Maalesef içerik bulunamadı. </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection