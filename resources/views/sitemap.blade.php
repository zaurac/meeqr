<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>https://www.meeqr.com/</loc>
        <lastmod>2024-01-06</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.8</priority>
    </url>

    <url>
        <loc>https://www.meeqr.com/sikca-sorulan-sorular</loc>
        <lastmod>2024-01-06</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.8</priority>
    </url>

    <url>
        <loc>https://www.meeqr.com/iletisim</loc>
        <lastmod>2024-01-06</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.8</priority>
    </url>

    <url>
        <loc>https://www.meeqr.com/kayit-ol</loc>
        <lastmod>2024-01-06</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.8</priority>
    </url>

    <url>
        <loc>https://www.meeqr.com/blog</loc>
        <lastmod>2024-01-06</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.8</priority>
    </url>

    @foreach($blog as $post)
        <url>
            <loc>{{ route('blog.detail',$post->slug) }}</loc>
            <lastmod>{{ empty($post->updatedAt) ? $post->createdAt : $post->updatedAt }}</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.8</priority>
        </url>
    @endforeach
</urlset>
