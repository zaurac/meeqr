@extends('layout.main')
@section('content')
    <div class="brandcrumb-area-2 breadcrumb-area-space-2" data-overlay="theme-2" data-opacity="7" data-background="{{asset('assets/images/faq.jpg')}}">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="breadcrumb-content text-center">
                        <h1 class="service-breadcrumb-title" data-color="#fff">Sıkça Sorulan Sorular </h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="faq-area pt-90 pb-90">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xxl-8 col-xl-8 col-lg-8">
                    <div class="faq-breadcrumb">
                        <div class="accordion" id="mainFaq">
                            @php $jsonLd = []; @endphp

                            @foreach($faq as $key => $question)
                                @php
                                    $accordionId = 'accordion-'.Str::slug($question->question, '-');
                                    $jsonLd[]    = ["@type" => "Question","name" => $question->question,"acceptedAnswer" => ["@type" => "Answer","text" => strip_tags(ckeditorImage($question->answer))]];
                                @endphp

                                <div class="sasup-accordion-item @if($key == 0) sasup-active-item @endif">
                                    <h3>
                                        <button type="button" @if($key > 0) class="collapsed"  @endif data-bs-toggle="collapse" data-bs-target="#{{$accordionId}}" aria-expanded="@if($key == 0) true @else false @endif" aria-controls="{{$accordionId}}">
                                            {{$question->question}}
                                        </button>
                                    </h3>
                                    <div id="{{$accordionId}}" class="collapse @if($key == 0) show @endif" data-bs-parent="#mainFaq">
                                        <div class="sasup-accordion-desc">{!! $question->answer !!}</div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "FAQPage",
            "mainEntity": {!! json_encode($jsonLd) !!}
        }
    </script>
@endsection