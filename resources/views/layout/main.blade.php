<!doctype html>
<html class="no-js" lang="tr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('assets/images/fav/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('assets/images/fav/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/images/fav/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('assets/images/fav/site.webmanifest')}}">
    <link rel="mask-icon" href="{{asset('assets/images/fav/safari-pinned-tab.svg')}}" color="#2A363B">
    <meta name="msapplication-TileColor" content="#EB4A5F">
    <meta name="theme-color" content="#EB4A5F">

    @php
        Artesaos\SEOTools\Facades\OpenGraph::setTitle(SEOMeta::getTitle());
        Artesaos\SEOTools\Facades\OpenGraph::setDescription(SEOMeta::getDescription());

        if(empty($noindex) || $noindex != 1)
        {
            SEOMeta::setCanonical(request()->url());
        }
    @endphp

    {!! SEOMeta::generate() !!}
    {!! OpenGraph::generate() !!}


    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/fontawesome-all.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/animate.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/magnific-popup.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/meanmenu.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/main.min.css')}}">
</head>

<body>

<header>
    <div class="header-transparent sasup-header-style-5 header-sticky">
        <div class="header-main">
            <div class="container">
                <div class="row align-items-center justify-content-between">
                    <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-4 col-6">
                        <div class="sasup-logo d-inline-block">
                            <a href="{{route('index')}}" class="logo-1">
                                {!! WebpConvert::createTag('assets/images/logo-light-1.png',['width' =>[151,118], 'height' => [50,50]],['alt' => settings('site.title'),'title' => settings('site.title'),'class' => 'h-auto'],null,settings('site.title').'-logo-light') !!}
                            </a>
                            <a href="{{route('index')}}" class="logo-2">
                                {!! WebpConvert::createTag('assets/images/logo-dark.png',['width' =>[161], 'height' => [50]],['alt' => settings('site.title'),'title' => settings('site.title'),'class' => 'h-auto'],null,settings('site.title').'-logo-dark') !!}
                            </a>
                        </div>
                    </div>
                    <div class="col-xxl-6 col-xl-6 col-lg-6 d-none d-lg-block text-center">
                        <div class="sasup-header d-none d-lg-inline-block">
                            <nav id="mobile-menu">
                                <ul>
                                    <li><a href="{{route('index')}}">Anasayfa</a></li>
                                    <li><a href="{{route('index')}}#fiyatlandirma">Fiyatlandırma</a></li>
                                    <li><a href="{{route('faq')}}">S.S.S</a></li>
                                    <li><a href="{{route('blog.index')}}">Blog</a></li>
                                    <li><a href="{{route('contact')}}">İletişim</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-8 col-6">
                        <div class="sasup-header-action-5 text-end">
                            <a href="{{env('APP_CUSTOMER_PANEL_URL')}}" target="_blank" class="sasup-login-btn-5 header-btn"><i class="fal fa-user"></i> Giriş Yap</a>
                            <a href="{{route('register')}}" class="sasup-header-white-btn-5 header-btn">MeeQR'a Katıl</a>
                            <div class="mobile-bar-control mobile-bar-control-white d-inline-block d-lg-none">
                                <div class="line"></div>
                                <div class="line"></div>
                                <div class="line"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="responsive-sidebar d-block d-lg-none">
    <div class="responsive-sidebar-inner">
        <div class="logo mb-30">
            <div class="row">
                <div class="col-6">
                    {!! WebpConvert::createTag('assets/images/logo-light-1.png',['width' =>[151,118], 'height' => [50,50]],['alt' => settings('site.title'),'title' => settings('site.title'),'class' => 'h-auto'],null,settings('site.title').'-logo-light') !!}
                </div>
                <div class="col-6">
                    <div class="text-end">
                        <a href="#" class="responsive-sidebar-close"><i class="fal fa-times"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="resposive-sidebar-menu mb-50">
            <div class="mobile-menu"></div>
        </div>
        <div class="responsive-sidebar-actions">
            <a href="{{env('APP_CUSTOMER_PANEL_URL')}}" target="_blank" class="sasup-border-btn d-block sasup-broder-btn-space-3 ms-0 text-center mb-2">Giriş Yap</a>
            <a href="{{route('register')}}" class="sasup-theme-btn text-center d-inline-block d-sm-none w-100">
                <span>MeeQR'a Katıl</span>
            </a>
        </div>
    </div>
</div>


<main>
    @yield('content')
</main>


<div class="progress-wrap">
    <svg class="progress-circle svg-content" width="100%" height="100%" viewBox="-1 -1 102 102">
        <path d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98" />
    </svg>
</div>

<footer class="footer-area py-3" data-bg-color="#2a373b" data-color="#ffffff">
    <div class=" container">
        <div class="footer-3">
            <div class="row">
                <div class="col-12 text-center"> Bütün Hakları Saklıdır. <a href="https://www.zaurac.io" target="_blank" class="text-white text-decoration-underline">zaurac</a></div>
            </div>
        </div>
    </div>
</footer>

<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.meanmenu.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.scrollUp.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('assets/js/mouse-wheel.min.js')}}"></script>
<script src="{{asset('assets/js/back-to-top.min.js')}}"></script>
<script src="{{asset('assets/js/main.min.js')}}"></script>
<script src="{{asset('assets/js/script.min.js')}}"></script>
</body>

</html>