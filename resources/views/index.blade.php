@extends('layout.main')
@section('content')
    <section class="hero-area hero-space-5 bg-top-center mainBanner" data-background="{{asset('assets/images/hero-bg-2.webp')}}" style="background-color: #2A363B">
        <div class="hero-area fix">
            <div class="container">
                <div class="sasup-banner-content-5 text-center row align-items-center mt-150">
                    <h1 class="sasup-banner-title-5 col-12" data-wow-delay=".6s">
                        Yeni Nesil Dijital<br>QR <span>Menü</span>
                    </h1>
                    <div class="sasup-banner-list-5 mb-40 col-12">
                        <ul>
                            <li class="bdevs-el-subtitle">
                                <i class="fal fa-check"></i> Yeni nesil teknoloji özellikleri
                            </li>
                            <li class="bdevs-el-subtitle">
                                <i class="fal fa-check"></i> %99 müşteri memnuniyeti
                            </li>
                            <li class="bdevs-el-subtitle">
                                <i class="fal fa-check"></i> Kolay Kullanım
                            </li>
                        </ul>
                    </div>
                    <div class="sasup-banner-action-btn-5 mb-60 col-12">
                        <a class="sasup-black-theme-btn-5" href="{{route('register')}}">14 günlük ÜCRETSİZ deneme sürenizi başlatın</a>
                    </div>
                    <div class="sasup-hero-image-group-5 group-image col-12">
                        <div class="sasup-hero-image-group-item-5-1">
                            {!! WebpConvert::createTag('assets/images/hero-4.webp',['width' =>[870,696,336], 'height' => [584,467,226]],['alt' => settings('site.title'),'title' => settings('site.title'),'class' => 'h-auto'],null,settings('site.title').'-hero-4') !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="service-area pb-85 fix">
        <div class="container">
            <div class="row mb-40 mt-40 mt-xl-0">
                <div class="col-xxl-12">
                    <div class="sasup-section-heading-5 wow fadeInRight" data-wow-delay=".4s">
                        <p class="sasup-section-heading-5-subtitle">Dijital Menü</p>
                        <h2 class="sasup-section-heading-5-title" data-wow-delay=".2s">MeeQR <br> <span>Avantajları</span> </h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="sasup-service-box-5 mb-30 text-center wow fadeInUp" data-wow-delay=".4s">
                        <div class="sasup-service-icon-5 icon">
                            <i class="fal fa-heart"></i>
                        </div>
                        <div class="sasup-service-content-5">
                            <h3 class="sasup-service-content-title-5">Hızlı ve Kolay Menü Erişimi</h3>
                            <p class="sasup-service-content">
                                Müşterilere anında ve kolayca menüye erişim sağlar, böylece kullanıcı deneyimini optimize eder.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="sasup-service-box-5 mb-30 text-center wow fadeInUp" data-wow-delay=".6s">
                        <div class="sasup-service-icon-5 icon">
                            <i class="fal fa-book-open"></i>
                        </div>
                        <div class="sasup-service-content-5">
                            <h3 class="sasup-service-content-title-5">
                                Görsel ve Bilgi Zengini Menü
                            </h3>
                            <p class="sasup-service-content">
                                Çekici görseller ve açıklayıcı metinlerle zenginleştirilmiş menüler sunarak müşterilere daha çekici bir deneyim sunar.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="sasup-service-box-5 mb-30 text-center wow fadeInUp" data-wow-delay=".8s">
                        <div class="sasup-service-icon-5 icon">
                            <i class="fal fa-lightbulb"></i>
                        </div>
                        <div class="sasup-service-content-5">
                            <h3 class="sasup-service-content-title-5">
                                Mobil Uyumlu ve Hızlı Yükleme
                            </h3>
                            <p class="sasup-service-content">
                                Mobil cihazlara uyumlu tasarımıyla müşterilere rahat ve hızlı bir kullanıcı deneyimi sunar. Bu özellik, müşterilerin menünüze kolayca erişmesini ve gezinmesini sağlar.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="features-area fix sasup-bg-gray-1 pt-115 pb-115 p-rel">

        <div class="container text-center p-rel">
            <div class="row mb-25">
                <div class="col-xxl-12">
                    <div class="section-heading wow fadeInUp" data-wow-delay=".3s">
                        <div class="sasup-section-heading-5 text-center">
                            <h2 class="sasup-section-heading-5-title">
                                Baskı İşlemlerinde Tasarruf
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sasup-image-features-dashbord-img-parent-wrap-2">
                <div class="sasup-image-features-dashbord-img-wrap-2">
                    <div class="sasup-feature-single-dashbord-img-1">
                        {!! WebpConvert::createTag('assets/images/shape/features-2-1-1-1.webp',['width' =>[800,556,514], 'height' => [612,425,393]],['alt' => settings('site.title'),'title' => settings('site.title'),'class' => 'h-auto'],null,settings('site.title').'-features') !!}
                    </div>

                </div>
            </div>
            <div class="sasup-feature-content-wrap-2 text-center">
                <h2 class="sasup-feature-content-title-2">Maliyetleri azaltın, baskı süreçlerinizi optimize ederek hızlı sonuçlar elde edin.</h2>
                <p class="sasup-feature-content-desc-2">Kolay ve hızlı bir başlangıç için iş süreçlerinizi hemen başlatın.</p>
                <a class="sasup-theme-started-btn" href="{{route('register')}}">Şimdi Başla</a>
            </div>
        </div>
    </div>

    <section class="advanced-features-area pt-115 pb-115">
        <div class="container">
            <div class="row">
                <div class="col-xxl-12">
                    <div class="sasup-section-heading-5 text-center mb-70 wow fadeInUp" data-wow-delay=".4s">
                        <h3 class="sasup-section-heading-5-subtitle">Gelişmiş özellikler</h3>
                        <h2 class="sasup-section-heading-5-title" data-wow-delay=".2s">
                            Lider teknoloji için amaca yönelik tasarlanan<br><span>Menü Tasarımı</span> ve QR Teknolojisi
                        </h2>
                    </div>
                </div>
            </div>
            <div class="sasup-timeline-wrap-1 sasup-timeline-num-0">
                <div class="sasup-timeline-item-single-1 active">
                    <span class="sasup-timeline-item-single-count active">1</span>
                    <div class="row">
                        <div class="col-xxl-6 col-xl-6 col-lg-6">
                            <div class="sasup-timeline-item-single-left-1 wow fadeInUp" data-wow-delay=".4s">
                                <div class="sasup-timeline-item-single-img-1">
                                    {!! WebpConvert::createTag('assets/images/timeline/timeline-1.webp',['width' =>[470,431,229], 'height' => [350,321,171]],['decoding' => 'async','alt' => settings('site.title'),'title' => settings('site.title'),'class' => 'h-auto'],null,settings('site.title').'-timeline-1') !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-6 col-xl-6 col-lg-6">
                            <div class="sasup-timeline-item-single-right-1 wow fadeInUp" data-wow-delay=".6s">
                                <div class="sasup-timeline-item-single-right-content-1">
                                    <h3 class="sasup-timeline-item-single-right-title-1">
                                        Hızlı ve Sorunsuz <br> Müşteri Deneyimi
                                    </h3>
                                    <p class="text-start sasup-timeline-item-single-right-desc-1">
                                        Müşterilere anında erişim sunarak restoran ve kafelerde sipariş süreçlerini hızlandırır. Müşteriler, masa başında QR kodunu tarayarak menüyü anında inceleyebilir, tercihlerini belirleyebilir ve siparişlerini kolayca verebilir. Bu hızlı ve sorunsuz deneyim, müşteri memnuniyetini artırırken, işletmenizin verimliliğini de optimize eder.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sasup-timeline-wrap-1 sasup-timeline-wrap-pos-reverce sasup-timeline-num-1">
                <div class="sasup-timeline-item-single-1">
                    <span class="sasup-timeline-item-single-count">2</span>
                    <div class="row">
                        <div class="col-xxl-6 col-xl-6 col-lg-6">
                            <div class="sasup-timeline-item-single-left-xs-1 wow fadeInUp" data-wow-delay=".4s">
                                <div class="sasup-timeline-item-single-right-content-1">
                                    <h3 class="sasup-timeline-item-single-right-title-1">
                                        Çevrimiçi Görünürlük ve Pazarlama
                                    </h3>
                                    <p class="sasup-timeline-item-single-right-desc-1 text-start">
                                        QR kodları, müşterilere çevrimiçi platformlarda hızlı ve kolay erişim sağlar. İşletmenizin web sitesi, sosyal medya hesapları veya diğer dijital kanallar aracılığıyla paylaşılan QR kodları, potansiyel müşterilere işletmenizi tanıtma ve menünüzü keşfetme imkanı sunar. Bu da işletmenizin çevrimiçi görünürlüğünü artırarak yeni müşteri kazanımına katkı sağlar.
                                    </p>
                                    <a class="sasup-gray-bordered-btn-1 text_btn"
                                       href="{{route('register')}}">Şimdi Başla</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-6 col-xl-6 col-lg-6">
                            <div class="sasup-timeline-item-single-left-1 wow fadeInUp" data-wow-delay=".6s">
                                <div class="sasup-timeline-item-single-img-1">
                                    {!! WebpConvert::createTag('assets/images/timeline/timeline-2.webp',['width' =>[470,431,229], 'height' => [350,321,171]],['decoding' => 'async','alt' => settings('site.title'),'title' => settings('site.title'),'class' => 'h-auto'],null,settings('site.title').'-timeline-2') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sasup-timeline-wrap-1 sasup-timeline-num-2">
                <div class="sasup-timeline-item-single-1 ">
                    <span class="sasup-timeline-item-single-count ">3</span>
                    <div class="row">
                        <div class="col-xxl-6 col-xl-6 col-lg-6">
                            <div class="sasup-timeline-item-single-left-1 wow fadeInUp" data-wow-delay=".4s">
                                <div class="sasup-timeline-item-single-img-1">
                                    {!! WebpConvert::createTag('assets/images/timeline/timeline-3.webp',['width' =>[470,431,229], 'height' => [350,321,171]],['decoding' => 'async','alt' => settings('site.title'),'title' => settings('site.title'),'class' => 'h-auto'],null,settings('site.title').'-timeline-3') !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-6 col-xl-6 col-lg-6">
                            <div class="sasup-timeline-item-single-right-1 wow fadeInUp" data-wow-delay=".5s">
                                <div class="sasup-timeline-item-single-right-content-1">
                                    <h3 class="sasup-timeline-item-single-right-title-1">
                                        Verimli İş Süreçleri ile İşletme Performansını Artırın
                                    </h3>
                                    <p class="sasup-timeline-item-single-right-desc-1 text-start">
                                        Dijitalleşme, iş süreçlerini optimize ederek işletme verimliliğini artırır. QR menü, sipariş süreçlerini hızlandırır, personel performansını iyileştirir ve müşteri memnuniyetini artırarak işletmenizin rekabet avantajını güçlendirir.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="priceing-area pt-115 pb-115 fix" id="fiyatlandirma">
        <div class="container">
            <div class="row mb-5">
                <div class="col-xxl-12">
                    <div class="sasup-section-heading-5 text-center wow fadeInUp" data-wow-delay=".4s">
                        <h3 class="sasup-section-heading-5-subtitle">Fiyatlandırmamız</h3>
                        <h2 class="sasup-section-heading-5-title" data-wow-delay=".2s">Sizin İçin <span>Doğru</span>  Fiyatlandırma</h2>
                    </div>
                </div>
            </div>

            <div class="advanced-pricing-widget-tab-content-wrapper-2">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show show active" id="monthly-tab-content0" role="tabpanel"
                         aria-labelledby="monthly-tab0">
                        <div class="row justify-content-center">
                            @php $jsonLdPackage = []; @endphp
                            @foreach($packages as $package)
                                @php
                                    $jsonLdPackage[] = ["@type" => "Product","name" => $package->title,"offers" => ["@type" => "Offer","priceCurrency" => 'EUR',"price" => $package->price]];
                                @endphp

                                <div class="col-xl-4 col-lg-4 col-md-6">
                                    <div class="sasup-pricing-box-2 mb-30">
                                        <div class="sasup-pricing-box-top-2">
                                            <span class="sasup-pricing-box-top-subtitle-2">14 Gün Ücretsiz</span>
                                            <h3 class="sasup-pricing-box-top-title-2">{{$package->title}}</h3>
                                            <a href="{{route('register')}}" class="sasup-pricing-box-top-action-2 ">Ücretsiz dene</a>
                                        </div>
                                        <div class="sasup-pricing-box-bottom-2">
                                            <div class="sasup-pricing-box-bottom-title-wrap-2">
                                                <p class="sasup-pricing-box-bottom-price-2 text-start">{{$package->price}} ₺</p>
                                            </div>
                                            <div class="sasup-pricing-box-bottom-list-2">
                                                {!! $package->content !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                                <script type="application/ld+json">
                                    {
                                        "@context": "https://schema.org",
                                        "@type": "ItemList",
                                        "url": "https://www.meeqr.com#fiyatlandirma",
                                        "numberOfItems": "315",
                                        "itemListElement": {!! json_encode($jsonLdPackage) !!}
                                    }
                                </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="cta-area">
        <div class="container">
            <div class="row">
                <div class="col-xxl-12">
                    <div class="sasup-cta-content-5">
                        <h2 class="sasup-cta-title-5">Denemeye hazır mısınız?<br> Ücretsiz denemenizi bugün başlatın.</h2>
                        <div class="sasup-cta-action-wrapper-5">
                            <a class="sasup-cta-action-link-5" href="{{route('register')}}">Şimdi başla</a>
                            <a class="sasup-cta-action-transparent-link-5" href="{{route('faq')}}">Daha fazla bilgi edin</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="faq-area pt-115 pb-115">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xxl-6 col-lg-6">
                    <div class="sasup-section-heading-5 text-center mb-40 wow fadeInUp" data-wow-delay=".4s">
                        <p class="sasup-section-heading-5-subtitle">Daha Fazla Öğren</p>
                        <h2 class="sasup-section-heading-5-title">Sıkça Sorulan Sorular</h2>
                        <P>Ürünlerimiz ve hizmetlerimizle ilgili sık sorulan soruların yanıtlarını bulmak için kapsamlı sık sorulan sorular (SSS) listemizi keşfedin</P>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-xxl-8 col-xl-8 col-lg-8">
                    <div class="faq-breadcrumb">
                        <div class="accordion" id="mainFaq">
                            @php $jsonLd = []; @endphp

                            @foreach($faq as $key => $question)
                                @php
                                    $accordionId = 'accordion-'.Str::slug($question->question, '-');
                                    $jsonLd[]    = ["@type" => "Question","name" => $question->question,"acceptedAnswer" => ["@type" => "Answer","text" => strip_tags(ckeditorImage($question->answer))]];
                                    $expanded    = $key == 0 ? 'true' : 'false';
                                @endphp

                                <div class="sasup-accordion-item @if($key == 0) sasup-active-item @endif">
                                    <h3>
                                        <button type="button" @if($key > 0) class="collapsed"  @endif data-bs-toggle="collapse" data-bs-target="#{{$accordionId}}" aria-expanded="{{$expanded}}" aria-controls="{{$accordionId}}">
                                            {{$question->question}}
                                        </button>
                                    </h3>
                                    <div id="{{$accordionId}}" class="collapse @if($key == 0) show @endif" data-bs-parent="#mainFaq">
                                        <div class="sasup-accordion-desc">{!! $question->answer !!}</div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="col-xxl-8 col-xl-8 col-lg-8 col-12 text-center mt-5">
                    <a href="{{route('faq')}}" class="primary-button">Hepsini Oku</a>
                </div>
            </div>
        </div>
    </div>
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "FAQPage",
            "mainEntity": {!! json_encode($jsonLd) !!}
        }
    </script>
    <script type="application/ld+json">{"@context":"http://schema.org","@type":"Organization","name":"Meeqr","alternatename":"MeeQr Dijital Menü","legalName":"Zaurac Technology","taxID":"40840281412","url":"https://www.meeqr.com","slogan":"Yeni Nesil Dijital Menü","numberOfEmployees":"3","description":"{{SEOMeta::getDescription()}}","logo":{"@type":"ImageObject","url":"https://www.meeqr.com/assets/images/logo-dark.png","height":"50","width":"161"},"contactPoint":[{"@type":"ContactPoint","telephone":"212-705-45-45","email":"merhaba@meeqr.com","contactOption":"TollFree","areaServed":"TR","availableLanguage":["Turkish","English"]}],"sameAs":["https://www.linkedin.com/company/zauracteknoloji","https://twitter.com/zauracteknoloji","https://www.instagram.com/zauractechnology","https://www.facebook.com/zauractechnology","https://www.youtube.com/@zauractechnology"],"address":{"@type":"PostalAddress","streetAddress":"Çamlık Mahallesi Emre Sokak No:24 Daire:7 Çekmeköy / İstanbul","addressLocality":"İstanbul","addressRegion":"Çekmeköy","postalCode":"34782","addressCountry":"TUR"},"foundingDate":"2016","foundingLocation":{"@type":"Place","address":{"@type":"PostalAddress","addressLocality":"İstanbul","addressCountry":"TUR"}},"founder":[{"@type":"Person","jobTitle":"Founder","name":"Muhammet Salih KAZAK"},{"@type":"Person","jobTitle":"Founder","name":"Halil İbrahim BELKIR"}]}</script>
@endsection